import { LogoutComponent } from './../logout/logout.component';
import { LoginComponent } from './../login/login.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  displayGal = false;
  displayLogin = true;
  constructor(
    public dialog: MatDialog,
    private snakeBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {}
  openLoginDialog = (): void => {
    this.router.navigate(['/']);
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (localStorage.getItem('validUser') == 'yes') {
        this.snakeBar.open('you are successfully logged in', 'Close', {
          duration: 2000,
        });
        this.displayGal = true;
        this.displayLogin = false;
      } else {
        this.displayGal = false;
        this.displayLogin = true;
      }
    });
  };
  openLogoutDialog = (): void => {
    const dialogRef = this.dialog.open(LogoutComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result == 'logout') {
        localStorage.clear();
        this.displayLogin = true;
        this.displayGal = false;
        this.snakeBar.open('you are successfully logged out', 'Close', {
          duration: 2000,
        });
      } else {
        this.displayLogin = false;
        this.displayGal = true;
      }
    });
  };
}
