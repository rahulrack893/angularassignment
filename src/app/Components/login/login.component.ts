import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import data from '../../../userData.json';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loading = false;
  loginForm = new FormGroup({
    userName: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });
  validationMessages = {
    userName: {
      required: 'User Name is requird',
    },
    password: {
      required: 'Password is required',
    },
  };
  formErrors = {
    userName: '',
    password: '',
  };
  constructor(
    public dialogRef: MatDialogRef<LoginComponent>,
    private snakeBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    /*colling log error*/
    this.loginForm.valueChanges.subscribe((data) => {
      this.logValidationError(this.loginForm);
    });
  }
  /*Checking form Errors*/
  logValidationError = (group: FormGroup) => {
    Object.keys(group.controls).forEach((ele) => {
      const fieldControls = group.get(ele);
      if (fieldControls instanceof FormGroup) {
        this.logValidationError(fieldControls);
      } else {
        this.formErrors[ele] = ''; //clear all privious error
        if (fieldControls && !fieldControls.valid) {
          const messages = this.validationMessages[ele];
          for (const errorKey in fieldControls.errors) {
            if (errorKey) {
              this.formErrors[ele] += messages[errorKey] + '';
            }
          }
        }
      }
    });
  };
  /**submit */
  submit = () => {
    if (this.loginForm.valid) {
      data.usersDB.forEach((element) => {
        if (element.userid == this.loginForm.value.userName) {
          let user = element;
          if (user.password == this.loginForm.value.password) {
            localStorage.setItem('validUser', 'yes');
            this.dialogRef.close();
          } else {
            localStorage.setItem('validUser', 'no');
          }
        } else {
          this.snakeBar.open('invalid User Id or Password ', 'Close', {
            duration: 2000,
          });
        }
      });
    }
  };
}
